#include "Hopper.h"

void Hopper::move()
{
	if (this->alive == true) 
	{
		//rotation
		while (isWayBlocked())
		{
			randomDirection();
		}
		//movement
		switch (this->direction)
		{
		case 1://north
			this->position.second -= this->hopLength;
			if (this->position.second < 0)
			{
				this->position.second = 0;
			}
			path.push_back(this->position);
			break;
		case 2://east
			this->position.first += this->hopLength;
			if (this->position.first > 10)
			{
				this->position.first = 10;
			}
			path.push_back(this->position);
			break;
		case 3://south
			this->position.second += this->hopLength;
			if (this->position.second > 10)
			{
				this->position.second = 10;
			}
			path.push_back(this->position);
			break;
		case 4://west
			this->position.first -= this->hopLength;
			if (this->position.first < 0)
			{
				this->position.first = 0;
			}
			path.push_back(this->position);
			break;
		}
	}
}
Hopper::Hopper(int id, pair<int, int> position, int direction, int size, int hopLength)
{
	this->id = id;
	this->position = position;
	this->direction = direction;
	this->size = size;
	this->hopLength = hopLength;
	this->alive = true;
}

ostream& operator<<(ostream& strm, const Hopper& hop)
{
	string alive;
	if (hop.alive == true) { alive = "Yes"; }
	else { alive = "No"; }
	return strm << "Id: " << hop.id << " Position: (" << hop.position.first << ","<<hop.position.second << ") Direction: " << hop.direction << " Size: " << hop.size << " HopLength: " << hop.hopLength << " Alive: "<< alive
		<<endl;
}