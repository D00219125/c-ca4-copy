#include "Crawler.h"

void Crawler::move()
{

	if (this->alive == true) 
	{

		//rotation
		while (isWayBlocked())
		{
			randomDirection();
		}
		//movement
		switch (this->direction)
		{
		case 1://north
			this->position.second -= 1;
			if (this->position.second < 0)
			{
				this->position.second = 0;
			}
			path.push_back(this->position);
			break;
		case 2://east
			this->position.first += 1;
			if (this->position.first > 10)
			{
				this->position.first = 10;
			}
			path.push_back(this->position);
			break;
		case 3://south
			this->position.second += 1;
			if (this->position.second > 10)
			{
				this->position.second= 10;
			}
			path.push_back(this->position);
			break;
		case 4://west
			this->position.first -= 1;
			if (this->position.first < 0)
			{
				this->position.first = 0;
			}
			path.push_back(this->position);
			break;
		}
	}
}

Crawler::Crawler(int id,pair<int,int> position, int direction, int size)
{
	this->id = id;
	this->position = position;
	this->direction = direction;
	this->size = size;
	this->alive = true;
}

ostream& operator<<(ostream& strm, const Crawler& craw)
{
	string alive;
	if (craw.alive == true) { alive = "Yes"; }
	else { alive = "No"; }
	return strm << "Id: " << craw.id << " Position: (" << craw.position.first << "," << craw.position.second << ") Direction: " << craw.direction << " Size: " << craw.size << " Alive: " << alive << endl;
}