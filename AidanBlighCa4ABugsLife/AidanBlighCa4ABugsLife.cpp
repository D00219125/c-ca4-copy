#include <string>
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
using namespace std;

void printMainMenu();
//q1
#include"Bug.h"
//----new classes-----
#include "Hopper.h"
#include "Crawler.h"
//----new classes-----
void initializeBoard();
void parseLine(const string& s);
vector<Hopper> hoppers;
vector<Crawler> crawlers;
//q2
void displayAllBugs(/*vector<Bug> const &hops, vector<Bug> const& craws*/);
//q3
void findBugsById();
void printSearchByIdOptions();
void findCrawlersById();
void findHoppersById();
//-----new code-----
vector<Bug> bugVec;
void findAnyById();
//-----new code-----
//q4
void tapBoard();
//q5
void printPathHistory();
//q6
void savePath();
//q7
void displayCells();
//q8 
void bugCombat();
int main()
{
	bool endMenu = false;
	while (!endMenu)
	{
		int answerAsInt = -1;
		string answer = "";

		try
		{
			//int choice = 0;
			printMainMenu();
			getline(cin, answer);
			answerAsInt = stoi(answer);
			//cin >> choice;
			switch (answerAsInt)
			{
				//find out if you can use enums
			case 1:
				initializeBoard();
				break;
			case 2:
				displayAllBugs(/*hoppers, crawlers*/);
				break;
			case 3:
				findBugsById();
				break;
			case 4:
				tapBoard();
				break;
			case 5:
				printPathHistory();
				break;
			case 6:
				displayCells();
				break;
			case 7:
				savePath();
				cout << "Enter 1 if you want to quit." << endl;
				string leave;
				cin >> leave;
				if (leave == "1") { endMenu = true; }
			}
		}
		catch (invalid_argument e)
		{
			cout << "something broke with the input, going back to main" << endl;
			main();
			endMenu = true;
		}

	}
	cout << "Thank you for using my Bug Simulator";
}

void printMainMenu()
{
	cout << "Enter 1 to initalize board.\n";
	cout << "Enter 2 to display all bugs.\n";
	cout << "Enter 3 to find a bug by id.\n";
	cout << "Enter 4 to tap the board.\n";
	cout << "Enter 5 to display path history of all bugs\n";
	cout << "Enter 6 to display all cells and their bugs.\n";
	cout << "Enter 7 to save and exit.\n";
}

void initializeBoard()
{
	string bug;
	ifstream bugList("bugs.txt");
	if (bugList.good())
	{
		while (getline(bugList, bug))
		{
			parseLine(bug);
		}
	}
	cout << "Board filled." << endl;
}
void parseLine(const string& s) 
{
	stringstream strstrm(s);
	string type;//cant use strstrm while this is a char
	int id, pos1, pos2, dir, size, hoplength;
	try 
	{
		string str;
		getline(strstrm, type, ';');
		//I can make this a switch if I make "C" and "H" constants, I will look into it when making enums
		if (type == "H") 
		{
			getline(strstrm, str, ';');
			id = stoi(str);
			getline(strstrm, str, ';');
			pos1 = stoi(str);
			getline(strstrm, str, ';');
			pos2 = stoi(str);
			pair<int, int> p{pos1,pos2};
			getline(strstrm, str, ';');
			dir = stoi(str);
			getline(strstrm, str, ';');
			size = stoi(str);
			getline(strstrm, str, ';');
			hoplength = stoi(str);
			Hopper h = Hopper(id,p,dir,size,hoplength);
			//cout << h;
			hoppers.push_back(h);
			bugVec.push_back(h);
		}
		else if (type == "C") 
		{
			getline(strstrm, str, ';');
			id = stoi(str);
			getline(strstrm, str, ';');
			pos1 = stoi(str);
			getline(strstrm, str, ';');
			pos2 = stoi(str);
			pair<int, int> p{ pos1,pos2 };
			getline(strstrm, str, ';');
			dir = stoi(str);
			getline(strstrm, str, ';');
			size = stoi(str);
			Crawler c = Crawler(id, p, dir, size);
			crawlers.push_back(c);
			bugVec.push_back(c);
			//cout << c;

		}
		else 
		{
			cout << "Something has gone wrong with the text file." << endl;
		}
	}
	catch (invalid_argument const& e)
	{
		cout << "invalid arguement occured" << endl;
	}
}

void displayAllBugs(/*vector<Bug> const& hops, vector<Bug> const& craws*/)
{
	if (crawlers.empty() && hoppers.empty()) 
	{
		cout << "There are no bugs in the system yet" << endl;
	}
	else 
	{
		cout << "-----crawlers-----" << endl;
		for (int i = 0; i < crawlers.size(); i++) 
		{
			cout << crawlers[i];
		}
		cout << "-----hoppers-----" << endl;
		for (int i = 0; i < hoppers.size(); i++)
		{
			cout << hoppers[i];
		}
	}
	/*if (bugVec.empty())
	{
		cout << "There are no bugs in the system yet" << endl;
	}
	else
	{
		cout << "Cant distinguish between crawlers and hoppers lol" << endl;
		for (int i = 0; i < bugVec.size(); i++)
		{
			cout << bugVec[i];
		}
	}*/
	
}

void findBugsById()
{
	int answerAsInt = -1;
	string answer = "";
	try
	{
		printSearchByIdOptions();
		getline(cin, answer);
		answerAsInt = stoi(answer);
		while (answerAsInt < 1 || answerAsInt>3)
		{
			cout << "Please enter an answer between 1 and 3" << endl;
			printSearchByIdOptions();
			getline(cin, answer);
			answerAsInt = stoi(answer);
			cin.sync();
		}
		switch (answerAsInt)
		{
		case 1:
			findCrawlersById();
			break;
		case 2:
			findHoppersById();
			break;
		case 3:
			findAnyById();
		}
	}
	catch (invalid_argument& e)
	{
		cout << "Something messed up with the buffer, Probably my fault. Try again" << endl;
		findBugsById();
	}

}

void printSearchByIdOptions()
{
	cout << "Enter 1 if you want to search for Crawlers by Id" << endl;
	cout << "Enter 2 if you want to search for Hoppers by Id" << endl;
	cout << "Enter 3 if you want to search through all bugs by Id" << endl;
	
}

void findCrawlersById()
{
	if (crawlers.empty()) 
	{
		cout << "Crawlers Vector is empty, returning to main menu" << endl;
	}
	else 
	{
		int answerAsInt = -1;
		string answer = "";
		try
		{
			cout << "Enter the Id of the Crawler you are searching for." << endl;
			getline(cin, answer);
			answerAsInt = stoi(answer);
			bool found = false;
			cout << "looking for " << answerAsInt << endl;
			for (int i = 0; i < crawlers.size(); i++) 
			{
				//cout << crawlers[i].getId() << endl;
				if (crawlers[i].getId() == answerAsInt) 
				{
					cout << crawlers[i];
					found = true;
				}
			}
			if (!found) { cout << "crawler not found" << endl;}
			else { cout << "crawler found" << endl; }
		}
		catch (invalid_argument& e)
		{
			cout << "Something messed up with the buffer, Id's don't contain characters. Try again" << endl;
			findCrawlersById();
		}
	}
}

void findHoppersById()
{
	if (hoppers.empty())
	{
		cout << "Hoppers Vector is empty, returning to main menu" << endl;
	}
	else
	{
		int answerAsInt = -1;
		string answer = "";
		try
		{
			cout << "Enter the Id of the Hopper you are searching for." << endl;
			getline(cin, answer);
			answerAsInt = stoi(answer);
			bool found = false;
			cout << "looking for " << answerAsInt << endl;
			for (int i = 0; i < hoppers.size(); i++)
			{
				//cout << hoppers[i].getId() << endl;
				if (hoppers[i].getId() == answerAsInt)
				{
					cout << hoppers[i];
					found = true;
				}
			}
			if (!found) { cout << "hopper not found" << endl;}
			else { cout << "hopper found" << endl; }
		}
		catch (invalid_argument& e)
		{
			cout << "Something messed up with the buffer, Id's don't contain characters. Try again" << endl;
			findHoppersById();
		}
	}
}

void findAnyById()
{
	if (bugVec.empty())
	{
		cout << "Bug Vector is empty, returning to main menu" << endl;
	}
	else
	{
		int answerAsInt = -1;
		string answer = "";
		try
		{
			cout << "Enter the Id of the Bug you are searching for." << endl;
			getline(cin, answer);
			answerAsInt = stoi(answer);
			bool found = false;
			cout << "looking for " << answerAsInt << endl;
			for (int i = 0; i < bugVec.size(); i++)
			{
				//cout << hoppers[i].getId() << endl;
				if (bugVec[i].getId() == answerAsInt)
				{
					cout << bugVec[i];
					found = true;
				}
			}
			if (!found) { cout << "bug not found" << endl; }
			else { cout << "bug found" << endl; }
		}
		catch (invalid_argument& e)
		{
			cout << "Something messed up with the buffer, Id's don't contain characters. Try again" << endl;
			findHoppersById();
		}
	}
}

void tapBoard()
{
	if (crawlers.empty() && hoppers.empty()) 
	{
		cout << "You are tapping an empty board." << endl;
	}
	for (int i = 0; i < crawlers.size();i++) 
	{
		crawlers[i].move();
	}
	for (int i = 0; i < hoppers.size(); i++)
	{
		hoppers[i].move();
	}
	bugCombat();

	cout << "Bugs have moved" << endl;
}
void printPathHistory()
{
	cout << "-----crawlers-----" << endl;
	for (int i = 0; i < crawlers.size(); i++)
	{
		cout << crawlers[i].getId() << ": ";
		for (pair<int, int> pos : crawlers[i].getPath()) 
		{
			cout << "(" << pos.first << "," << pos.second << ")" << ",";
		}
		cout << endl;
	}
	cout << "-----hoppers-----" << endl;
	for (int i = 0; i < hoppers.size(); i++)
	{
		cout << hoppers[i].getId() << ": ";
		for (pair<int, int> pos : hoppers[i].getPath())
		{
			cout << "(" << pos.first << "," << pos.second << ")" << ",";
		}
		cout << endl;
	}
}

void savePath()
{
	ofstream writePath("Bugs_Life_Out.txt");
	string line;
	if (writePath.is_open()) 
	{
		writePath << "-----crawlers-----" << endl;
		for (int i = 0; i < crawlers.size(); i++)
		{
			writePath << crawlers[i].getId() << ": ";
			for (pair<int, int> pos : crawlers[i].getPath())
			{
				writePath << "(" << pos.first << "," << pos.second << ")" << ",";
			}
			writePath << endl;
		}
		writePath << "-----hoppers-----" << endl;
		for (int i = 0; i < hoppers.size(); i++)
		{
			writePath << hoppers[i].getId() << ": ";
			for (pair<int, int> pos : hoppers[i].getPath())
			{
				writePath << "(" << pos.first << "," << pos.second << ")" << ",";
			}
			writePath << endl;
		}
		cout << "File was successfully written to." << endl;
	}
	else { cout << "File was not successfully written to!" << endl; }
}


void displayCells()
{
	for (int x = 0; x < 11; x++) 
	{
		for (int y = 0; y < 11; y++) 
		{
			string tempX = to_string(x);
			string tempY = to_string(y);
			cout << "Cell(" + tempX + "," + tempY + ")";
			string crawlreCell;
			string hopperCell;
			for (int i = 0; i < crawlers.size(); i++) 
			{
				if (crawlers[i].getPos().first == x && crawlers[i].getPos().second == y) 
				{
					cout << "Crawler ";
					cout <<crawlers[i].getId();
				}
			}
			for (int i = 0; i < hoppers.size(); i++)
			{
				if (hoppers[i].getPos().first == x && hoppers[i].getPos().second == y)
				{
					cout << "\tHopper ";
					cout <<hoppers[i].getId();
				}
			}
			cout << endl;
		}
	}
}

/*
Rules: Bugs can eat their own type, bigger sized bug wins, equal sized bugs both live
*/
void bugCombat()
{
	//crawler vs hopper
	for (int i = 0; i < crawlers.size(); i++)
	{
		for (int j = 0; j < hoppers.size(); j++)
		{
			
			if (crawlers[i].getPos() == hoppers[j].getPos()) 
			{
				if (crawlers[i].getSize()>hoppers[j].getSize() && hoppers[j].isAlive() == true) 
				{
					hoppers[j].killed();
					cout << hoppers[j].getId() << "was killed by " << crawlers[i].getId() << endl;
				}
				else if (crawlers[i].getSize() < hoppers[j].getSize() && crawlers[i].isAlive() == true)
				{
					crawlers[i].killed();
					cout << crawlers[i].getId() << "was killed by " << hoppers[j].getId() << endl;
				}
				else if(crawlers[i].getSize() == hoppers[j].getSize() && crawlers[i].isAlive() == true)
				{
					cout << crawlers[i].getId() << " faught with " << hoppers[j].getId() << " both survived." << endl;
				}
			}
		}
	}
	//crawler vs crawler
	for (int i = 0; i < crawlers.size(); i++)
	{
		for (int j = 0; j < crawlers.size(); j++)
		{

			if (crawlers[i].getPos() == crawlers[j].getPos())
			{
				if (crawlers[i].getSize() > crawlers[j].getSize() && crawlers[i].getId() != crawlers[j].getId() && crawlers[j].isAlive() == true)
				{
					crawlers[j].killed();
					cout << crawlers[j].getId() << "was killed by " << crawlers[i].getId() << endl;
				}
				else if (crawlers[i].getSize() < crawlers[j].getSize() && crawlers[i].getId() != crawlers[j].getId() && crawlers[i].isAlive() == true)
				{
					crawlers[i].killed();
					cout << crawlers[i].getId() << "was killed by " << crawlers[j].getId() << endl;
				}
				else if(crawlers[i].getId() != crawlers[j].getId() && crawlers[i].isAlive() == true && crawlers[j].isAlive() == true)
				{
					cout << crawlers[i].getId() << " faught with " << crawlers[j].getId() << " both survived." << endl;
				}
			}
		}
	}
	//hopper vs hopper
	for (int i = 0; i < hoppers.size(); i++)
	{
		for (int j = 0; j < hoppers.size(); j++)
		{

			if (hoppers[i].getPos() == hoppers[j].getPos())
			{
				if (hoppers[i].getSize() > hoppers[j].getSize() && hoppers[i].getId() != hoppers[j].getId() && hoppers[j].isAlive() == true)
				{
					hoppers[j].killed();
					cout << hoppers[j].getId() << "was killed by " << hoppers[i].getId() << endl;
				}
				else if (hoppers[i].getSize() < hoppers[j].getSize() && hoppers[i].getId() != hoppers[j].getId() && hoppers[i].isAlive() == true)
				{
					hoppers[i].killed();
					cout << hoppers[i].getId() << "was killed by " << hoppers[j].getId() << endl;
				} 
				else if(hoppers[i].getId() != hoppers[j].getId() && hoppers[j].isAlive() == true && hoppers[j].isAlive() == true)
				{
					cout << hoppers[i].getId() << " faught with " << hoppers[j].getId() << " both survived." << endl;
				}
			}
		}
	}
}
