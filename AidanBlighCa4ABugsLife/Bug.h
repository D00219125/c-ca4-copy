#pragma once
using namespace std;
#include <utility>
#include <list>
#include <iostream>
#include <fstream>
#include<string>
class Bug 
{
protected:
	int id;
	pair<int, int> position;
	int direction;//replace with enum
	int size;
	bool alive = true;
	list<pair<int, int>> path;
	//virtual void move();
	bool isWayBlocked();
	//need to find out how to make this work
	friend ostream& operator<<(ostream& strm, const Bug& bug);
	//void randomDirection();
public:
	bool isAlive();
	bool killed();
	int getSize();
	pair<int, int> getPos();
	list<pair<int, int>> getPath();
	int getId();
	void randomDirection();
	virtual void move();
//public:
//	friend ostream& operator<<(ostream& strm, const Crawler& craw);
//	friend ostream& operator<<(ostream& strm, const Hopper& hop);
};

/*	Moving the classses to their own .h like you said
class Crawler : public Bug
{
public:
	int getId();
	void move();
	Crawler(int id, pair<int, int> position, int direction, int size);
	friend ostream& operator<<(ostream& strm, const Crawler& craw);
};
class Hopper : public Bug 
{
public:
	int getId();
	int hopLength;
	void move();
	Hopper(int id, pair<int, int> position, int direction, int size, int hopLength);
	friend ostream& operator<<(ostream& strm, const Hopper& hop);
};*/